/**
 * Created by Shlomi on 29/05/2016.
 */

import gulp from 'gulp';
import pluginsFn from 'gulp-load-plugins';
import fs from 'graceful-fs';
import path from 'path';

import paths from './build/gulp/paths.js';
import './build/gulp/aliases.js';

let options = {};
let plugins = pluginsFn();

try{
    // Load all tasks
    fs
        .readdirSync(paths.gulp.tasks)
        .forEach(function(filename){

            let file = path.join(paths.gulp.tasks, filename);
            let stat = fs.statSync(file);

            if (stat.isFile() && filename.slice(-3) !== '.js') {
                return;
            }

            let name = filename.slice(0, -3);
            plugins.util.log('Register task: '+name);
            let task = require(`${paths.gulp.tasks}/${filename}`).default(gulp, plugins, options);

            // Register the task
            gulp.task(`prv-${name}`, task);
        });
}catch(e){
    plugins.util.log(e);
}
