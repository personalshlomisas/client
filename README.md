# Palo Alto Networks Exam - Client

## Clone the project

run `git clone git@bitbucket.org:personalshlomisas/client.git &&  cd client`

## Install

run `npm install`

## Build

run `npm run build`

## Start

run `npm start`