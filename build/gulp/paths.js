/**
 * Created by Shlomi on 07/08/2015.
 */

import path from 'path';

let paths = {
    base: {
        src: './src',
        dest: './dest'
    },
    babel: {
        get src(){
            return path.join(paths.base.src, '**/*.js');
        },
        get dest(){
            return paths.base.dest;
        }
    },
    gulp: {
        base: './build/gulp',
        tasks: './build/gulp/tasks'
    }
};

export default paths;
