/**
 * Created by Shlomi on 07/08/2015.
 */

import path from 'path';
import paths from '../paths';

export default (gulp, plugins)=>{
    return ()=>{
        return gulp
            .src(path.join(paths.base.src, '**/*.js'))

            .pipe(plugins.sourcemaps.init())
            .pipe(plugins.babel({
                "plugins": ["transform-class-properties"],
                "presets": [
                    "stage-0",
                    "es2015"
                ]
            }))
            .pipe(plugins.sourcemaps.write('.'))
            .pipe(gulp.dest(paths.base.dest));
    }
};