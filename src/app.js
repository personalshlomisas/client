/**
 * Created by Shlomi on 29/05/2016.
 */

// import socket
import helper from './utils/helper';
import Client from './utils/client';
import Player from './utils/player';
import Move from './utils/move';

let client = new Client();

client.on('connect', () => {

    helper.debug(`Client has connected, register events..`);

    // Client listeners
    client.on('server:ready', () => {

        helper.debug('Server is ready, creating player and ask to join the game..');

        let player = new Player();
        helper.error(`Player ${player} has been created`);

        // Player listeners
        player.on('move:made', move => {
            client.emit('move:made', {
                player: player.toJSON(),
                move: move.toJSON()
            });
        });

        client.on('move:make', data => {
            if(!data || !data.player) return;
            player.onMakeMove(new Player(data.player));
        });

        client.on('move:made', data => {
            if(!data || !data.player || !data.move) return;
            player.onMoveMade(new Player(data.player), new Move(data.move));
        });

        client.on('game:ended', data => {
            if(!data || !data.player || !data.move) return;
            player.onMoveMade(new Player(data.player), new Move(data.move));
        });

        client.on('myError', err => {
            helper.error(`Got error from server: ${err}`);
        });

        client.emit('game:join', {
            player: player.toJSON()
        });
    });

    client.on('game:ended', scores => {
        helper.debug(`Game has ended`);
        helper.printScores(scores);
        helper.debug(`Exiting..`);
        helper.exit();
    });

    client.emit('socket:ready');
});

client.on('disconnect', () => {
    helper.debug('Socket has disconnected, exiting..');
    helper.exit();
});
