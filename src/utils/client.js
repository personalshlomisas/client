/**
 * Created by Shlomi on 29/05/2016.
 */

import socketIOClient from 'socket.io-client';

import config from '../config';

export default class Client{

    _sioClient;

    constructor(){
        const sioSettings = config.get('socketio');
        const sioPath = `ws://${sioSettings.host}:${sioSettings.port}`;

        this._sioClient = socketIOClient(sioPath);
    }

    on(){
        this._sioClient.on(...arguments);
    }

    emit(){
        this._sioClient.emit(...arguments);
    }
    
}