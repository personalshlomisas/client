export default {

    log(){
        console.log(...arguments);
    },

    debug(){
        this.log(...arguments);
    },

    error(){
        console.error(...arguments);
    },

    getRandomInt(max, min = 0){
        return Math.floor(Math.random() * (max - min + 1)) + min;
    },

    exit(e){
        if(e) this.error(e);
        process.exit();
    },

    getWeightedRand(spec) {
        var i, j, table=[];
        for (i in spec) {
            // The constant 10 below should be computed based on the
            // weights in the spec for a correct and optimal table size.
            // E.g. the spec {0:0.999, 1:0.001} will break this impl.
            for (j=0; j<spec[i]*10; j++) {
                table.push(i);
            }
        }

        return table[Math.floor(Math.random() * table.length)];
    },

    printScores(scores){
        this.debug(`Final scores:`);
        Object.keys(scores).forEach(key => {
            this.debug(`Player ${key}: ${scores[key]}`);
        });
    }
}