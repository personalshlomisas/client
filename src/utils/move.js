/**
 * Created by Shlomi on 29/05/2016.
 */

import helper from './helper';
import config from '../config';

const ROLL_ACTION = 'roll';
const HOLD_ACTION = 'hold';

export default class Move{

    static _actions = [ROLL_ACTION, HOLD_ACTION];
    static _cube = [1, 2, 3, 4, 5, 6];

    _action;
    _number;

    constructor(data = {}){

        const spec = {};

        spec[ROLL_ACTION] = config.get('game:rollActionProbability');
        spec[HOLD_ACTION] = config.get('game:holdActionProbability');

        this._action = data.action || helper.getWeightedRand(spec);
        
        if(this._action === ROLL_ACTION){
            this._number = data.number || Move._cube[helper.getRandomInt(Move._cube.length - 1)];
        }
    }

    toJSON(){
        return {
            action: this._action,
            number: this._number
        }
    }

    toString(){
        let str = `Move - ${this._action}`;
        if(this._action === HOLD_ACTION) return str;
        return `${str}, number: ${this._number}`;
    }

}
