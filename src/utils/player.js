/**
 * Created by Shlomi on 29/05/2016.
 */

import {EventEmitter} from 'events';

import helper from './helper';
import Move from './move';

export default class Player extends EventEmitter{

    _name;

    constructor(data){
        super();
        this._name = (data ? data.name : undefined) || Player.generateRandomName();
    }

    toJSON(){
        return {
            name: this._name
        }
    }

    toString(){
        return `Player ${this._name}`;
    }

    getName(){
        return this._name;
    }

    onMakeMove(player){
        if(!player) return;

        if(player.getName() !== this.getName()){
            helper.debug(`Player ${player} require to make move..`);
            return;
        }

        // My turn
        helper.debug(`You are making a move`);

        this.emit('move:made', new Move());

    }

    onMoveMade(player, move){
        if(!player || !move) return;
        helper.debug(`Player ${player} made a move ${move}`);
    }

    static generateRandomName() {

        let chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        let length = 10;
        let result = '';

        for (let i = length; i > 0; --i) {
            result += chars[Math.floor(Math.random() * chars.length)];
        }
        return result;
    }
}
